######### Создание актива на базе последней загруженной модели, заполнение метаданных, сохранение в базе. ########
# ПРИМЕЧАНИЕ: Масштаб текста на экране (настройка Windows) и масштаб экрана в настройках IE должны быть 100%

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Ie('C:\\Selenoid\\IEDriverServer_Win32_3.9.0\\IEDriverServer.exe')
driver.get('http://10.0.3.104:8084/')


driver.implicitly_wait(4)

######## Выбор раздела "Активы" ########
alm = driver.find_element_by_xpath('//a[@href="/assets"]')
alm.click()
driver.implicitly_wait(1)


######## Кнопка Добавить актив ########

driver.find_element_by_xpath('//span[contains(text(),"Добавить актив")]').click()

######## Кнопка "Из модели" ########

driver.find_element_by_xpath('//button[contains(text(),"Из модели")]').click()
driver.implicitly_wait(2)

######## Раскрыть выпадающий список, Выбор последней загруженной модели, Добавить ########
element = driver.find_element_by_xpath('//tdr-add-asset-unit-list/div/kendo-dropdownlist')
element.click()
element.send_keys(Keys.DOWN, Keys.RETURN)


driver.find_element_by_xpath('//button[contains(text(),"Добавить")]').click()

######## Клик на открытой модели (инчаче активна подмодель) ########
driver.find_element_by_class_name('b-assets-tree-item__entity_current').click()

##### Вес #####
weight = driver.find_element_by_xpath('//tdr-add-assets/div/div[2]/div[3]/div[2]/tdr-asset-edit/div/div[2]/input[1]')
weight.click()
weight.send_keys('1000')

##### Толщинометрия, базовая #####
tlsh = driver.find_element_by_xpath('//tdr-add-assets/div/div[2]/div[3]/div[2]/tdr-asset-edit/div/div[2]/input[2]')
tlsh.click()
tlsh.send_keys('40')


##### Толщинометрия, текущая #####
tlsht = driver.find_element_by_xpath('//tdr-add-assets/div/div[2]/div[3]/div[2]/tdr-asset-edit/div/div[2]/input[3]')
tlsht.click()
tlsht.send_keys('22')


##### № #####
num = driver.find_element_by_xpath('//div/tdr-add-assets/div/div[2]/div[3]/div[2]/tdr-asset-edit/div/div[1]/input[2]')
num.click()
num.send_keys('123456')


##### № по тех. схеме #####
numt = driver.find_element_by_xpath('//tdr-add-assets/div/div[2]/div[3]/div[2]/tdr-asset-edit/div/div[1]/input[3]')
numt.click()
numt.send_keys('а654321')

##### Детализация #####
leftelement = driver.find_element_by_class_name('b-asset-edit__left')
detalizaciya = leftelement.find_element_by_class_name('k-dropdown')
detalizaciya.click()
detalizaciya.send_keys(Keys.DOWN, Keys.DOWN, Keys.DOWN, Keys.RETURN)

##### Ранг #####
rightelement = driver.find_element_by_class_name('b-asset-edit__right')
rang = rightelement.find_element_by_class_name('k-dropdown')
rang.click()
rang.send_keys(Keys.DOWN, Keys.DOWN, Keys.RETURN)


##### кнопка Сохранить #####
driver.find_element_by_xpath('//button[contains(text(),"Сохранить")]').click()


driver.implicitly_wait(10)
driver.__exit__()