########## Загрузка в базу "РЕПОЗИТОРИЙ 3D" модели объекта, заполнение метаданных, сохранение в базе. ##########
### ПРИМЕЧАНИЕ: Масштаб текста на экране (настройка ОС) и масштаб экрана в настройках IE_11 должны быть 100% ###

import datetime
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Ie('C:\\Selenoid\\IEDriverServer_Win32_3.9.0\\IEDriverServer.exe')
driver.get('http://10.0.3.104:8084/')


def send_keys_to_iframe_field(value):
    element = driver.find_element_by_tag_name('iframe')
    element.click()
    element.send_keys(value)


driver.implicitly_wait(4)

######## Выбор раздела Модели ########
alm = driver.find_element_by_xpath('//a[@href="/units"]')
alm.click()
driver.implicitly_wait(4)


######## Кнопка Добавить модель ########
alm1 = driver.find_element_by_xpath('//span[contains(text(),"Добавить модель")]')
alm1.click()
driver.implicitly_wait(4)


######## Выбор файла модели ########
inputElement = driver.find_element_by_name('files')
inputElement.send_keys('C:\\temp\\Objects\\RVS_2000.fbx')


######## Кнопка Далее ########
driver.find_element_by_class_name('b-load-unit__next').click()
driver.implicitly_wait(4)
today = datetime.datetime.today()
dt = today.strftime("%Y/%m/%d-%H.%M")


######## Классификатор ########
driver.find_element_by_class_name('categories-treeview__popup__button').click()
driver.find_element_by_xpath('//span[contains(text(),"Нефтедобыча")]').click()


######## Добавление даты к наименованию модели ########
byText = driver.find_element_by_xpath("//input[@type='text']")
byText.send_keys(Keys.END, '_', dt)
driver.implicitly_wait(4)

######## Характеристики модели ########
send_keys_to_iframe_field(dt)
driver.implicitly_wait(4)
#driver.switch_to.frame(driver.find_element_by_tag_name('iframe'))
#e70 = driver.find_element_by_xpath('/html/body')
#e70.send_keys(dt)
#driver.switch_to.default_content()


######## Сохранить ########
driver.find_element_by_class_name('b-load-unit__next').click()

time.sleep(7)  # т.к. при мгновенном закрытии браузера модель на превью не видна
driver.implicitly_wait(4)
driver.__exit__()
